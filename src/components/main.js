import React, {Component} from 'react';
import { Button } from 'reactstrap'
import { Image } from 'react-bootstrap'
import bitbucket from '../images/bitbucket.png'
import linkedin from '../images/linkedin4.png'
import twitter from '../images/twitter.png'
class Main extends Component {

    handleBitbucketClick(){
      this.props.history.push("www.google.com")
    }
    render(){
        const background = { backgroundSize: 'cover'}
        const titleStyle = {
            fontSize: '600%',
            color: 'white'
        }
         const nameStyle = {
            fontSize: '400%',
            color: 'white'
         }
         const containerStyle ={
             position: 'absolute',
             top: '20%',
             left: '25%',
             textAlign: 'center'
         }
        return(
            <div style={{ width: 'auto' }}>
                <Image
                    style = {background} responsive
                    src = "http://touchechicago.com/wp-content/uploads/2013/10/background-grey.jpg">
                </Image>
                <div style= {containerStyle}>
                  <h1 style={titleStyle}>Marorerwa Hervé Carol</h1>
                  <h2 style={nameStyle}> Software engineering student </h2>
                  <h3>École de Technologie Supérieur</h3>
                  <div>
                    <a target='_blank' href="https://bitbucket.org/carolherve/"><img src = {bitbucket} alt="bitbucket"/></a>
                    <a target='_blank' href="https://ca.linkedin.com/in/herve-carol-marorerwa-556586108"><img src = {linkedin} alt="linkedin"/></a>
                    <a target='_blank' href="https://twitter.com/hervecarol"><img src = {twitter} alt="twitter"/></a>
                  </div>
                </div>
            </div>
        );
    }
}

const iconStyle={

}
export default Main