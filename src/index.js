import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import Main from '../src/components/main';
import { render } from 'react-dom'
import { Router, Route } from 'react-router'
import { createHashHistory } from 'history'
const history = createHashHistory()


render((
  <Router history={history}>
    <Route path="/" component={Main}/>
  </Router>
), document.getElementById('root'))



